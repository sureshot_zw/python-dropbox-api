# Dropbox API

## The Idea
I designed a simle CLI application that consumes the Dropbox API. 
The CLI is multi threaded with the backup and restore functionality running off a seperate thread and not the main thread.
The application connects to Dropbox and uploads all files that are in the dropbox upload/download folder if there are any. 
If any file in the folder is deleted it will be restored from the version in the online Dropbox Folder. 
The application checks for changes after every 5 seconds. In the event that the application does backup an existing file, it will simply overwrite the file.

## How to get it working
### Install it first
The assumption is that you have Python 3 installed on your computer.
You will need to run the following commands to install the required modules:
`pip3 install -U python-dotenv`
`pip3 install -U click`
`pip3 install dropbox`

### Dropbox Access Token Creation
Next you will need to create an Access Token in Dropbox. To do this you have to visit the [Dropbox App Console](https://www.dropbox.com/developers/apps) and create a new application.
Once the application is created, Click the "Generate Access Token" button and this will generate an access token. Make a `.env` file using `.env.example` file as your template i.e. `cp .env.example .env`. Copy the access token into the `.env` environmental file in the projects root folder, on the `DROPBOX_ACCESS_KEY` property. Please ensure the following permissions are set for the application (done from the App Console):
* `files.metadata.write`
* `files.metadata.read`
* `files.content.write`
* `files.content.read`

## Using the application
At this point you simply have to run `python3 dropbox_cli.py` which will create the `dropbox_files` folder which is where files will be backed up or restored from. You can change this location in the environmental file of course. 

Copy a test file (can be any file really) into the `dropbox_files` folder and the application should pick up the file and back it up to Dropbox which you can confirm by visiting the folder in Dropbox. If you delete the file you added to the `dropbox_files` folder, the application should restore it from Dropbox. The application will only restore files it determines are not actually present in the folder. 

Exiting the Application is as simple as `Ctrl + C`

## With More time, Ideas I would like to explore
* Use of meta data to determine if we should backup/restore a file or not instead of just using a file name. 
* Having backup and restore functions running on seperate threads. 
* Allowing the user the opportunity to select of a revision to restore.
* Rate limited emailing when backups occurs. Email possibly periodically with a list of files that have changed. 