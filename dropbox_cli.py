import os
import time
import click
import logging
import threading
from src.Dropbox import Dropbox as Dropbox
from src.FileManager import FileManager as FileManager

@click.command()

# restore backups from dropbox (future change to have this run off its own thread)
def restoreBackups(arg):
    while not arg["stop"]:
        if len(os.listdir(fileManager.getFolderLocation())) == 0:
            dropbox.restore()

        time.sleep(5)

# backup files to drop box
def backupAndRestore(arg):
    while not arg["stop"]:
        dropbox.backup()

        if len(os.listdir(fileManager.getFolderLocation())) == 0:
            dropbox.restore()

        time.sleep(5)

if __name__ == "__main__":
    print("🙂🙂Dropbox API CLI Command🙂🙂")
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(relativeCreated)6d %(threadName)s %(message)s"
    )

    dropbox     = Dropbox()
    fileManager = FileManager()
    fileManager.ensureDropBoxFileLocation()


    info = {"stop": False}
    thread = threading.Thread(target=backupAndRestore, args=(info,))
    thread.start()

    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            info["stop"] = True
            logging.debug('Stopping')
            break

    thread.join()