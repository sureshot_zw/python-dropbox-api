import os

class FileManager:
    def __init__(self):
        self.folderLocation = os.getenv("DROPBOX_FOLDER")

    # ensure dropbox file location
    def ensureDropBoxFileLocation(self):
        if not os.path.exists(self.folderLocation):
            os.makedirs(self.folderLocation)

    #drop box file location
    def getFolderLocation(self):
        return self.folderLocation
