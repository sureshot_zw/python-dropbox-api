import os
import dropbox
import settings
from dropbox.files import WriteMode
from dropbox.exceptions import ApiError, AuthError
from src.FileManager import FileManager as FileManager

class Dropbox:
    def __init__(self):
        self.accessToken = os.getenv("DROPBOX_ACCESS_KEY")
        self.folderLocation = os.getenv("DROPBOX_FOLDER")
        self.dbx = dropbox.Dropbox(self.accessToken)
        self.fileManager = FileManager()

        # ensure we have fetched the token
        self.ensureAccessToken()

        # populate names for files already in dropbox to avoid uneeded API calls
        self.backupedFiles = self.getFilesnamesInDropbox()

        try:
            # connect to dropbox using the access token
            self.dbx.users_get_current_account()
        except AuthError:
            sys.exit("ERROR: Invalid access token; try re-generating an "
                "access token from the app console on the web.")

    # ensure drobox access token
    def ensureAccessToken(self):
        if (len(self.accessToken) == 0):
            sys.exit("ERROR: Looks like you didn't add your access token.")

    # Uploads contents of filename to Dropbox
    def backup(self):
        directory = self.fileManager.getFolderLocation()

        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            with open(directory + "/" + filename, 'rb') as f:
                #check if the file is already backed up to avoid unnecessary API calls
                if("/" + filename.lower() in self.backupedFiles):
                    print("%s is already backed up, skipping the file" % (filename))
                    continue

                name, extension =  os.path.splitext(filename)
                name += "_backup"
                backupFileName = "/%s%s" % (name, extension)
                print("Uploading %s %s to Dropbox as " % (filename, backupFileName))
                
                try:
                    self.dbx.files_upload(f.read(), backupFileName, mode=WriteMode('overwrite'))
                    self.backupedFiles.append("/" + filename.lower())
                except ApiError as err:
                    if (err.error.is_path() and
                            err.error.get_path().reason.is_insufficient_space()):
                        sys.exit("ERROR: Cannot back up; insufficient space.")
                    elif err.user_message_text:
                        print(err.user_message_text)
                        sys.exit()
                    else:
                        print(err)
                        sys.exit()

    # Restore backups that are missing
    def restore(self, dirPath = ""):
        entries = self.dbx.files_list_folder('').entries

        if len(entries) == 0:
            print('We could not find any backups.')

            return

        for entry in entries:
            fullPath = dirPath + entry.path_lower

            # only restore files we are sure are not currently in the folder
            if(os.path.isfile(self.fileManager.getFolderLocation() + fullPath) == False):
                print("Restoring %s" % (fullPath))
                self.dbx.files_download_to_file(self.fileManager.getFolderLocation() + fullPath.replace("_backup", ""), entry.path_lower)
            else:
                print("Not restoring %s as the file already exists at the location" % (fullPath))

    #get filenames of files currently in dropbox
    def getFilesnamesInDropbox(self, dirPath = ""):
        filenames = []
        entries = self.dbx.files_list_folder('').entries

        for entry in entries:
            filenames.append(entry.path_lower)

        return filenames